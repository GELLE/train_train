use Mix.Config

# Configure your database
config :train_train,
       TrainTrain.Repo,
       username: System.get_env("POSTGRES_USER") || "train_train_user",
       password: System.get_env("POSTGRES_PASSWORD") || "train_train_password",
       database: System.get_env("POSTGRES_DB") || "train_train_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",
       pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :train_train_web,
       TrainTrainWeb.Endpoint,
       http: [
         port: 4002
       ],
       server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Train Ticket App
config :train_ticket,
       TrainTicket.EventStore,
       serializer: Commanded.Serialization.JsonSerializer,
       username: System.get_env("POSTGRES_USER") || "postgres",
       password: System.get_env("POSTGRES_PASSWORD") || "traintrain",
       database: System.get_env("POSTGRES_DB_EV") || "train_ticket_ev_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",
       pool_size: 1

config :train_ticket,
       TrainTicket.Repo,
       username: System.get_env("POSTGRES_USER") || "postgres",
       password: System.get_env("POSTGRES_PASSWORD") || "traintrain",
       database: System.get_env("POSTGRES_DB_PROJ") || "train_train_projections_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",
       pool: Ecto.Adapters.SQL.Sandbox

config :train_ticket, :log_writer, TrainTicket.Test.LogWriter
