defmodule Mix.Tasks.Start.Db do
  use Mix.Task
  @moduledoc "Start the database for local tests or phoenix run"

  @shortdoc "Start the container with the database (train_train_postgres)"
  def run(_) do
    IO.puts("[Docker DB] Starting train_train_postgres database container")
    System.cmd("docker", ["start", "train_train_postgres"])
    :timer.sleep(2000)
    IO.puts("[Docker DB] started")
  end
end
