defmodule Mix.Tasks.Stop.Db do
  use Mix.Task
  @moduledoc "Start the database for local tests or phoenix run"

  @shortdoc "Stop the container with the database (train_train_postgres)"
  def run(_) do
    IO.puts("[Docker DB] Stopping train_train_postgres database container")
    System.cmd("docker", ["stop", "train_train_postgres"])
    IO.puts("[Docker DB] stopped")
  end
end
